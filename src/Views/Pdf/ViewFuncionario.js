import React, { Component } from 'react';

import NavAdmin from '../../Components/Navs/NavAdmin';

import { PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';
import PdfFuncionario from './PdfFuncionario'

import axios from 'axios'

class ViewFuncionario extends Component {

    state = {
        funcionario: []
    }

    getFuncionario = async () => {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/pdf/funcionario`)
        this.setState({ funcionario: res.data })
    }

    async componentDidMount() {
        this.getFuncionario()
        // console.log(this.state.users)
    }

    

    render() {
        // console.log(this.state.funcionario)
        return (
            <div className='containerAdmin' style={{ minHeight: "100vh" }}>

                <NavAdmin></NavAdmin>

                funcionario

                <PDFViewer style={{ width: "100%", height: "90vh" }} >
                    <PdfFuncionario fun={this.state.funcionario} />
                </PDFViewer>

            </div>
        )
    }
}

export default ViewFuncionario

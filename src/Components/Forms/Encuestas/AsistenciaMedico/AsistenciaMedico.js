import React from 'react';
import { Form, Row, Col } from 'react-bootstrap'
import Select from 'react-select'

const AsistenciaMedico = (props) => {

  console.log(props)
  
  const options = [
    { value: 'Ninguna', label: 'Ninguna'},
    { value: 'SURA', label: 'SURA' },
    { value: 'COOMEVA', label: 'COOMEVA' },
    { value: 'Salud Colmena', label: 'Salud Colmena' },
    { value: 'Salud Total', label: 'Salud Total' },
    { value: 'Cafesalud', label: 'Cafesalud' },
    { value: 'Sanitas', label: 'Vanilla' },
    { value: 'Medimas', label: 'Vanilla' },
    { value: 'Colseguros', label: 'Colseguros' },
    { value: 'Colpatria', label: 'Colpatria' },
    { value: 'Cruz Blanca', label: 'Cruz Blanca'},
    { value: 'Sisben', label: 'Sisben'},
    { value: 'Sanidad Militar', label: 'Sanidad Militar'},
    { value: 'Red Vital', label: 'Red Vital'} 
]
  return (
    <Form>
      <Form.Group as={Row} className="mb-3" controlId="Nombre">
        <Form.Label column sm="2">
          Nombre
        </Form.Label>
        <Col sm="10">
          <Form.Control type="text" placeholder="Nombre Completo" />
        </Col>
      </Form.Group>

      <Form.Group as={Row} className="mb-3" controlId="Documento">
        <Form.Label column sm="2">
          Documento
        </Form.Label>
        <Col sm="10">
          <Form.Control type="number" placeholder="documento" />
        </Col>
      </Form.Group>

      <Form.Group as={Row} className="mb-3" controlId="Email">
        <Form.Label column sm="2">
          Email
        </Form.Label>
        <Col sm="10">
          <Form.Control type="email" placeholder="email@ejemplo.com" />
        </Col>
      </Form.Group>

      <Form.Group as={Row} className="mb-3" controlId="telefono">
        <Form.Label column sm="2">
          Telefono
        </Form.Label>
        <Col sm="10">
          <Form.Control type="number" placeholder="Telefono" />
        </Col>
      </Form.Group>

      <Form.Group as={Row} className="mb-3" controlId="Direccion">
        <Form.Label column sm="2">
          Direccion
        </Form.Label>
        <Col sm="10">
          <Form.Control type="text" placeholder="Direccion" />
        </Col>
      </Form.Group>

      <Form.Group as={Row} className="mb-3" controlId="formPlaintextEmail">
        <Form.Label column sm="2">
          EPS
        </Form.Label>
        <Col sm="10">
          <div className="AsistenciaMedico">
            <Select
              options={options}
              // onChange={OnDropdownChange}
              defaultValue={options[0]}
            />
          </div>
        </Col>
      </Form.Group>
    </Form>
  );
}

export default AsistenciaMedico;

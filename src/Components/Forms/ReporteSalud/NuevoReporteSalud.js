
import React from 'react';
import {Form, Container, Row, Col, Alert, Button} from 'react-bootstrap'
import Swal from 'sweetalert2';
import axios from 'axios';
import { async } from 'rxjs';
import './estilos.css';

const ReporteSalud = (props) => {
    
    const mostrarAlerta1=()=>{
        Swal.fire({
          title: 'Seleccionaste todas tus respuestas?',
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonText: `Si, enviar`,
          cancelButtonText: `No, responder`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            enviar()
            Swal.fire({
                title: '¡Genial!',
                text: 'Gracias por confirmar tus sintomas y llenar la encuesta',
                confirmButtonText: 'Siguiente',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result)=>{
                if(result.isConfirmed){
                    Swal.fire({
                        title: 'Confirmacion',
                        text: 'Enviamos una pequeña recomendacion tomando en cuenta tus respuestas',
                        timer: 3000,
                        timerProgressBar: true,
                        showConfirmButton: false
                    }).then(()=>{
                        window.location.replace('');
                    })
                }
            })

          } else if (result.isDenied) {
            Swal.fire('Selecciona los sintomas que presentas', '', 'info')
          }
        });
      }

    const [fiebre, setFiebre] = React.useState(false)
    const [dolorTragar, setDolorTragar] = React.useState(false)
    const [Tos, setTos] = React.useState(false)
    const [dificultadRespirar, setDificultadRespirar] = React.useState(false)
    const [malestargeneral, setMalestarGeneral] = React.useState(false)
    const [gripa, setGripa] = React.useState(false)
    const [diarrea, setDiarrea] = React.useState(false)
    const [contacto, setContacto] = React.useState(false)
    const [tratamiento, setTratamiento] = React.useState(false)


    

    const cedula = props.cedula;
    console.log(cedula);

    const enviar = async e => {

        // e.preventDefault()

        const data = [
            "fiebre",
            "Tos",
            "dolorTragar",
            "malestargeneral",
            "dificultadRespirar",
            "gripa",
            "diarrea",
            "contacto",
            "tratamiento",
        ]

        let resultados = []


        data.forEach((item, index) => {
            let check = document.querySelectorAll(`[name="${item}"]`)

            check.forEach((e) => {
                if (e.checked) {
                    if (e.value === "si") {
                        // console.log("si")
                        resultados.push(e.value)
                    } else {
                        // console.log("no")
                        resultados.push(e.value)
                    }
                }
            })
        })

        resultados.forEach((item, index) => {
            console.log(item, index)
        })

        
        await axios.post(`${process.env.REACT_APP_API_URL}/api/email/covid`, {
            cedula: cedula,
            resultado: resultados
        })

        await axios.post(`${process.env.REACT_APP_API_URL}/api/email/covidAp`, {
            cedula: cedula,
            resultado: resultados
        })

        await axios.post(`${process.env.REACT_APP_API_URL}/api/encuestas/covid`, {
            cedula: cedula,
            resultado: resultados
        })

    }

    return (
        <div>
            <div className="card-body">
                <Container>
                    <h3>Presenta algunos de estos sintomas sintomas?</h3>
                    <hr/>
                    <Form onSubmit={enviar}>
                        <Row className='row-reporte'>
                            <Col>
                                <Form.Label>
                                    <strong>Fiebre?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setFiebre(e.target.value = true)}
                                            value={fiebre} name={'fiebre'} label={'Si'} value="si"/>
                                <Form.Check type="radio" onChange={e => setFiebre(e.target.value = false)}
                                            value={fiebre} name={'fiebre'} label={'No'} value="no"/>
                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Tos?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setTos(e.target.value = true)}
                                            value={Tos} name={'Tos'} label={'Si'} value="si"/>
                                <Form.Check type="radio" onChange={e => setTos(e.target.value = false)}
                                            value={Tos} name={'Tos'} label={'No'} value="no"/>
                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Dolor al tragar?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setDolorTragar(e.target.value = true)}
                                            value={dolorTragar} name={'dolorTragar'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setDolorTragar(e.target.value = false)}
                                            value={dolorTragar} name={'dolorTragar'} label={'No'} value="no" />
                            </Col>
                        </Row>
                        <Row className='row-reporte'>
                            <Col>
                                <Form.Label>
                                    <strong>Malestar general?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setMalestarGeneral(e.target.value = true)}
                                            value={malestargeneral} name={'malestargeneral'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setMalestarGeneral(e.target.value = false)}
                                            value={malestargeneral} name={'malestargeneral'} label={'No'} value="no" />
                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Dificultad para respirar?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setDificultadRespirar(e.target.value = true)}
                                            value={dificultadRespirar} name={'dificultadRespirar'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setDificultadRespirar(e.target.value = false)}
                                            value={dificultadRespirar} name={'dificultadRespirar'} label={'No'} value="no" />

                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Gripa?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setGripa(e.target.value = true)}
                                            value={gripa} name={'gripa'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setGripa(e.target.value = false)}
                                            value={gripa} name={'gripa'} label={'No'} value="no" />
                            </Col>
                        </Row>
                        <Row className='row-reporte'>
                            <Col>
                                <Form.Label>
                                    <strong>Diarrea?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setDiarrea(e.target.value = true)}
                                            value={diarrea} name={'diarrea'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setDiarrea(e.target.value = false)}
                                            value={diarrea} name={'diarrea'} label={'No'} value="no" />
                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>A tenido contacto con casos sospechosos o confirmados?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setContacto(e.target.value = true)}
                                            value={contacto} name={'contacto'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setContacto(e.target.value = false)}
                                            value={contacto} name={'contacto'} label={'No'} value="no" />

                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Se encuentra en tratamiento por enfermedad actual S?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setTratamiento(e.target.value = true)}
                                            value={tratamiento} name={'tratamiento'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setTratamiento(e.target.value = false)}
                                            value={tratamiento} name={'tratamiento'} label={'No'} value="no" />
                            </Col>
                        </Row>
                        <Row>
                        <div className="App">
                            <button type="button" class="btn btn-success" onClick={enviar} onClick={() => mostrarAlerta1()}>Registrar</button>
                        </div>
                        </Row>
                    </Form>
                </Container>
            </div>
        </div>
    )
}
export default ReporteSalud